export const imageData = [
  require("./assets/Adventure.jpg"),

  require("./assets/Arts.jpg"),

  require("./assets/HistoryCulture.jpg"),

  require("./assets/LeisureRecreation.jpg"),

  require("./assets/NatureWildlife.jpg"),

  require("./assets/Others.jpg"),
];

export const imageDataFood = [
  require("./assets/cafe.jpg"),

  require("./assets/hawker.jpg"),

  require("./assets/restaurant.jpg"),

  require("./assets/othersfood.jpg"),
];
