import firebase from "firebase";
import "firebase/auth";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyBGMDmKKCY7FtUN7kkQSKwujPBHaH6WTuQ",
  authDomain: "nixaton-1b8a7.firebaseapp.com",
  projectId: "nixaton-1b8a7",
  storageBucket: "nixaton-1b8a7.appspot.com",
  messagingSenderId: "189600661453",
  appId: "1:189600661453:web:5e573aa9be9d9972d78e84",
};

let app;
if (firebase.apps.length === 0) {
  app = firebase.initializeApp(firebaseConfig);
} else {
  app = firebase.app();
}

const db = firebase.firestore();
const auth = firebase.auth();

export { db, auth };
