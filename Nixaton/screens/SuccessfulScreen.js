import React from "react";
import LottieView from "lottie-react-native";

const SuccessfulScreen = ({ navigation }) => {
  setTimeout(() => {
    navigation.replace("Main");
  }, 2000);

  return (
    <>
      <LottieView
        style={{
          flex: 1,
        }}
        source={require("../assets/success.json")}
        autoPlay
        loop={true}
        speed={1}
      />
    </>
  );
};

export default SuccessfulScreen;
