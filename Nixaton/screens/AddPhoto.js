import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  Image,
  KeyboardAvoidingView,
  View,
} from "react-native";
import { Input, Button } from "react-native-elements";
import { auth, db } from "../Firebase";
import firebase from "firebase";
import { v4 as uuidv4 } from "uuid";

const AddPhoto = ({ navigation, route }) => {
  const [title, setTitle] = useState("");
  const [caption, setCaption] = useState("");
  const image = route.params?.image;

  const uploadFav = () => {
    db.collection("favorites")
      .doc(auth.currentUser.uid)
      .collection("favItems")
      .doc(uuidv4())
      .set({
        timestamp: firebase.firestore.FieldValue.serverTimestamp(),
        favItem: {
          result: { name: title, desccription: caption },
        },
        favItemImage: image,
      })
      .then(() => {
        navigation.replace("Profile");
      })
      .catch((err) => alert(err));

    setCaption("");
    setTitle("");
  };

  return (
    <KeyboardAvoidingView
      style={{
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        padding: 20,
      }}
    >
      <Image
        source={{ uri: image }}
        style={{ height: 200, width: 200, marginBottom: 20 }}
      />
      <Input
        placeholder="Add a Title"
        value={title}
        onChangeText={(text) => setTitle(text)}
      />
      <Input
        placeholder="Add a Caption"
        value={caption}
        onChangeText={(text) => setCaption(text)}
      />
      <Button title="Upload Image" onPress={uploadFav} />
      <View style={{ height: 50 }} />
    </KeyboardAvoidingView>
  );
};

export default AddPhoto;

const styles = StyleSheet.create({});
