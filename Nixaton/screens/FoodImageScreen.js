import React from "react";
import {
  TouchableOpacity,
  View,
  Image,
  Text,
  StyleSheet,
  ImageBackground,
  ScrollView,
} from "react-native";
import { imageDataFood } from "../ImageData";

const foodImageScreen = ({ navigation, route }) => {
  return (
    <ScrollView>
      {route.params.list.map((category, index) => {
        return (
          <TouchableOpacity
            key={index}
            activeOpacity={0.7}
            style={styles.touchable}
            onPress={() =>
              navigation.navigate("Category", {
                category,
                type: route.params.type,
              })
            }
          >
            <View style={styles.view}></View>
            <ImageBackground source={imageDataFood[index]} style={styles.image}>
              <Text style={styles.text}>{category}</Text>
            </ImageBackground>
          </TouchableOpacity>
        );
      })}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  view: {
    position: "absolute",
  },
  image: {
    resizeMode: "contain",
    width: 400,
    height: 220,
    opacity: 0.9,
  },
  touchable: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
  },
  text: {
    color: "white",
    fontSize: 40,
    fontWeight: "300",
    textAlign: "center",
    paddingTop: 90,
  },
});

export default foodImageScreen;
