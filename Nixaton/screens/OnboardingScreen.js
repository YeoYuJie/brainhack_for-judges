import React, { useState } from "react";
import { StyleSheet, View, Text, Image, ImageBackground } from "react-native";
import AppIntroSlider from "react-native-app-intro-slider";

const slides = [
  {
    key: "one",
    title: "Welcome to vTour",
    text: "The Ideal Virtual. \nDiscover Singapore from the comfort of your home !",
    image: require("../assets/onboard1.jpg"),
    backgroundColor: "#59b2ab",
  },
  {
    key: "two",
    title: "Features",
    text: "Share and find hidden places of interest around your neighbourhood\n\nUsing our virtual tour application, view attractions from the comfort of your home\n\nRate and review places for others to see",
    image: require("../assets/onboard2.jpg"),
    backgroundColor: "#febe29",
  },
  {
    key: "three",
    title: "What are you waiting for ?",
    text: "Register a free account today!",
    image: require("../assets/onboard3.jpg"),
    backgroundColor: "#22bcb5",
  },
];

export default function OnboardingScreen({ navigation }) {
  const [showLogin, setShowLogin] = useState(false);

  const renderItem = ({ item }) => {
    return (
      <ImageBackground
        source={item.image}
        style={{
          flex: 1,
          resizeMode: "cover",
          alignItems: "center",
          justifyContent: "flex-end",
          paddingBottom: 140,
          backgroundColor: "rgba(0,0,0, 0.50)",
        }}
        imageStyle={{ opacity: 0.9 }}
      >
        <View style={styles.textContainer}>
          <Text style={styles.title}>{item.title}</Text>
          <Text style={styles.text}>{item.text}</Text>
        </View>
      </ImageBackground>
    );
  };
  onDone = () => {
    setShowLogin(true);
  };
  if (showLogin) {
    navigation.replace("Login");
    return null;
  } else {
    return (
      <AppIntroSlider renderItem={renderItem} data={slides} onDone={onDone} />
    );
  }
}

const styles = StyleSheet.create({
  textContainer: {
    position: "absolute",
    bottom: 60,
    left: 20,
    width: 280,
    backgroundColor: "rgba(0,0,0, 0.40)",
    borderRadius: 10,
  },
  title: {
    color: "white",
    fontWeight: "bold",
    fontSize: 40,
    position: "relative",
    top: 10,
    paddingHorizontal: 20,
    paddingRight: 50,
  },
  text: {
    color: "white",
    marginTop: 40,
    fontSize: 18,
    paddingHorizontal: 20,
    paddingBottom: 20,
  },
});
