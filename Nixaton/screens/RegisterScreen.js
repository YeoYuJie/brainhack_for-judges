import React, { useState, useLayoutEffect } from "react";
import {
  StyleSheet,
  View,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Platform,
  Keyboard,
} from "react-native";
import { Text, Input, Button } from "react-native-elements";
import { auth } from "../Firebase";
import LottieView from "lottie-react-native";
import * as Google from "expo-google-app-auth";
import { FontAwesome5 } from "@expo/vector-icons";
import Colours from "../components/Theme/Colours";

const RegisterScreen = ({ navigation }) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [email, setEmail] = useState("");

  const register = () => {
    if (password !== confirmPassword) {
      alert("Passwords do not match!");
    } else {
      auth
        .createUserWithEmailAndPassword(email, password)
        .then((authUser) => {
          authUser.user.updateProfile({
            displayName: username,
            photoURL:
              "https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png",
          });
        })
        .catch((err) => alert(err.message));
    }
  };

  const signInWithGoogleAsync = async () => {
    try {
      const result = await Google.logInAsync({
        androidClientId:
          "189600661453-5p9vdpuqt4bnrs6luih9uvajv6h9darg.apps.googleusercontent.com",
        behavior: "web",
        iosClientId:
          "189600661453-jrhle6tt1u2vv5m45ts0koh8dppae1b0.apps.googleusercontent.com",
        scopes: ["profile", "email"],
      })
        .then(() => navigation.navigate("Success"))
        .catch((err) => alert(err));

      if (result.type === "success") {
        return result.accessToken;
      } else {
        return { cancelled: true };
      }
    } catch (e) {
      return { error: true };
    }
  };

  return (
    <>
      <LottieView
        style={{
          flex: 1,
          transform: [{ scaleX: 1.2 }, { scaleY: 1.3 }],
          position: "absolute",
          top: 13,
          right: 77,
          width: "55%",
        }}
        source={require("../assets/register.json")}
        autoPlay
        loop={true}
        speed={1}
      />
      <KeyboardAvoidingView
        // behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={styles.container}
      >
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <>
            <Text
              h3
              style={{ marginBottom: 20, paddingTop: 135, paddingBottom: -150 }}
            >
              Create an Account!
            </Text>
            <View style={styles.inputContainer}>
              <Input
                inputContainerStyle={styles.input}
                placeholder="Username"
                autofocus
                type="text"
                onChangeText={(text) => setUsername(text)}
                value={username}
              />
              <Input
                placeholder="Email"
                inputContainerStyle={styles.input}
                type="email"
                onChangeText={(text) => setEmail(text)}
                value={email}
              />
              <Input
                placeholder="Password"
                inputContainerStyle={styles.input}
                type="password"
                secureTextEntry
                onChangeText={(text) => setPassword(text)}
                value={password}
              />
              <Input
                placeholder="Confirm Password"
                inputContainerStyle={styles.input}
                type="password"
                secureTextEntry
                onChangeText={(text) => setConfirmPassword(text)}
                value={confirmPassword}
              />
            </View>
          </>
        </TouchableWithoutFeedback>
        <TouchableOpacity
          raised
          title="Register"
          onPress={register}
          style={styles.button}
        >
          <Text style={styles.signupText}>Sign Up</Text>
        </TouchableOpacity>
        <Text style={styles.text}>Or</Text>
        <FontAwesome5.Button
          style={styles.googleButton}
          name="google"
          onPress={signInWithGoogleAsync}
          justifyContent="center"
        >
          <Text style={styles.googleText}>Log In With Google</Text>
        </FontAwesome5.Button>
      </KeyboardAvoidingView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    zIndex: 2,
  },
  inputContainer: {
    width: 300,
    marginTop: 20,
  },
  input: {
    borderBottomColor: "black",
  },
  button: {
    width: 200,
    marginTop: 30,
    alignItems: "center",
    backgroundColor: Colours.accent,
    padding: 12,
    borderRadius: 10,
    marginBottom: 30,
  },
  signupText: {
    fontSize: 20,
    fontWeight: "400",
    color: "black",
  },

  text: {
    fontSize: 20,
    fontWeight: "400",
    bottom: 15,
  },

  googleButton: {
    width: 200,
    height: 45,
    borderRadius: 10,
    color: Colours.primary,
  },
  googleText: {
    color: "white",
    fontSize: 17,
  },
});

export default RegisterScreen;
