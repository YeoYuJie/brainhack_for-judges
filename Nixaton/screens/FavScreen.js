import React, { useLayoutEffect, useState, useEffect } from "react";
import { StyleSheet, Text, ScrollView, View, Dimensions } from "react-native";
import { Image, Overlay, Input, Card, Button } from "react-native-elements";
import { createStackNavigator } from "@react-navigation/stack";
import InfoScreen from "./InfoScreen";
import { auth, db } from "../Firebase";
// import { API_KEY } from "../config";
import { API_KEY } from "@env";
import { IconButton } from "react-native-paper";
import * as ImagePicker from "expo-image-picker";
import { v4 as uuidv4 } from "uuid";
import firebase from "firebase";
import Icon from "react-native-vector-icons/MaterialIcons";
import Colours from "../components/Theme/Colours";

const FavsScreen = ({ navigation }) => {
  const [favs, setFavs] = useState([]);
  const [newFavName, setNewFavName] = useState("");
  const [newFavDescription, setNewFavDescription] = useState("");
  const [visible, setVisible] = useState(false);
  const [image, setImage] = useState(null);

  // DIMENSIONS
  const { width } = Dimensions.get("window");
  const adjWidth = width * 0.75;
  const height = width * 0.6;

  // PICK IMAGE FUNCTIONALITY
  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    console.log(result);

    if (!result.cancelled) {
      setImage(result.uri);
    }
  };

  // CONFIGURE HEADER
  useLayoutEffect(() => {
    navigation.setOptions({
      title: "Your Favorites",
      headerRight: () => (
        <IconButton
          icon="plus-circle-outline"
          onPress={toggleOverlay}
          style={{ marginRight: 15 }}
        />
      ),
    });
  }, [navigation]);

  // GET IMAGES AND DETAILS
  useEffect(() => {
    const unsubscribe = db
      .collection("favorites")
      .doc(auth.currentUser.uid)
      .collection("favItems")
      .onSnapshot((snapshot) => {
        setFavs(
          snapshot.docs.map((doc) => ({
            id: doc.id,
            data: doc.data(),
          }))
        );
      });
    return unsubscribe;
  }, [navigation, favs]);

  // DELETE FAV FUNCTIONALITY
  const deleteFav = (id) => {
    db.collection("favorites")
      .doc(auth.currentUser.uid)
      .collection("favItems")
      .doc(id)
      .delete()
      .catch((err) => alert(err));
  };

  // DISPLAY FAVORITES
  let displayFavs;

  if (favs.length === 0) {
    displayFavs = <Text>No Favorites!</Text>;
  }

  displayFavs = favs.map((fav, index) => (
    <Card key={index} containerStyle={{ borderRadius: 10 }}>
      <View style={{ alignItems: "center" }}>
        <Card.Title>{fav.data.favItem.result.name}</Card.Title>
        <Image
          source={{
            uri:
              `${fav.data.favItemImage}?apikey=${API_KEY}` ||
              fav.data.favItemImage,
          }}
          style={{
            height,
            width: adjWidth,
          }}
        />
      </View>
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          justifyContent: "flex-end",
        }}
      >
        <Button
          icon={<Icon name="info" size={24} color={Colours.accent} />}
          buttonStyle={{ backgroundColor: "white" }}
          onPress={() => navigation.navigate("Info", fav.data.favItem)}
        />
        <Button
          icon={<Icon name="delete" size={24} color={Colours.tertiary} />}
          buttonStyle={{ backgroundColor: "white" }}
          onPress={() => deleteFav(fav.id)}
        />
      </View>
    </Card>
  ));

  //TOGGLE OVERLAY
  const toggleOverlay = () => {
    setVisible(!visible);
  };

  //UPLOAD NEW FAVORITE
  const uploadFav = () => {
    db.collection("favorites")
      .doc(auth.currentUser.uid)
      .collection("favItems")
      .doc(uuidv4())
      .set({
        timestamp: firebase.firestore.FieldValue.serverTimestamp(),
        favItem: {
          result: { name: newFavName, desccription: newFavDescription },
        },
        favItemImage: image,
      })
      .catch((err) => alert(err));

    setNewFavDescription("");
    setNewFavName("");
    setImage(null);
  };

  return (
    <ScrollView style={styles.container}>
      <Overlay
        isVisible={visible}
        onBackdropPress={toggleOverlay}
        overlayStyle={styles.overlay}
      >
        <View>
          <Input
            placeholder="Name of Favorite"
            value={newFavName}
            onChangeText={(text) => setNewFavName(text)}
          />
          <Input
            placeholder="Description"
            value={newFavDescription}
            onChangeText={(text) => setNewFavDescription(text)}
          />
          <Button
            title="Pick an Image"
            onPress={pickImage}
            buttonStyle={{ backgroundColor: Colours.primary }}
          />
          {image && (
            <Image
              source={{ uri: image }}
              style={{ width: 200, height: 200 }}
            />
          )}
          <Button
            title="Upload Favorite"
            onPress={uploadFav}
            containerStyle={{ marginTop: 5 }}
            buttonStyle={{ backgroundColor: Colours.primary }}
          />
        </View>
      </Overlay>
      <View>{displayFavs}</View>
    </ScrollView>
  );
};

const FavScreen = () => {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator>
      <Stack.Screen name="Fav" component={FavsScreen} />
      <Stack.Screen name="Info" component={InfoScreen} />
    </Stack.Navigator>
  );
};

export default FavScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  overlay: {
    minWidth: 300,
    padding: 10,
  },
  button: {
    width: 70,
  },
});
