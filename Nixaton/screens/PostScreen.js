import React from "react";
import { Text, ImageBackground } from "react-native";

function PostScreen({ navigation, route }) {
  return (
    <ImageBackground
      source={route.params.imageURL}
      style={{ flex: 1, justifyContent: "center" }}
    >
      <Text
        style={{
          fontSize: 20,
          color: "white",
          textAlign: "center",
          lineHeight: 50,
          fontWeight: "bold",
          color: "#FFF",
          borderRadius: 12,
          overflow: "hidden",
          backgroundColor: "rgba(0,0,0, 0.50)",
        }}
      >
        {route.params.name}
      </Text>
    </ImageBackground>
  );
}

export default PostScreen;
