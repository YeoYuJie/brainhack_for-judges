import React, { useLayoutEffect } from "react";
import { createStackNavigator } from "@react-navigation/stack";
import InfoScreen from "./InfoScreen";
import CategoryScreen from "./CategoryScreen";
import ImageScreen from "./ImageScreen";

const Stack = createStackNavigator();

const attractionsList = [
  "Adventure",
  "Arts",
  "History & Culture",
  "Leisure & Recreation",
  "Nature & Wildlife",
  "Others",
];

function HomeScreen() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Categories"
        component={ImageScreen}
        options={{ title: "Choose a Category" }}
        initialParams={{ list: attractionsList, type: "attractions" }}
        options={{ headerLeft: null }}
      />
      <Stack.Screen
        name="Category"
        component={CategoryScreen}
        options={{ title: "Choose an Attraction" }}
      />
      <Stack.Screen name="Info" component={InfoScreen} />
    </Stack.Navigator>
  );
}

export default HomeScreen;
