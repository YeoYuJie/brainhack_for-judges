import React, { useState, useEffect } from "react";
import {
  View,
  KeyboardAvoidingView,
  StyleSheet,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
  Image,
  TouchableOpacity,
} from "react-native";
import { Input, Button, Text } from "react-native-elements";
import { auth } from "../Firebase";
import LottieView from "lottie-react-native";
import Colours from "../components/Theme/Colours";

const LoginScreen = ({ navigation }) => {
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged((authUser) => {
      if (authUser) {
        navigation.replace("Success");
      }
    });

    return unsubscribe;
  }, []);

  const login = () => {
    auth
      .signInWithEmailAndPassword(email, password)
      .then(() => navigation.navigate("Main"))
      .catch((err) => alert(err.message));
  };

  return (
    <>
      <LottieView
        style={{
          flex: 1,
          transform: [{ scaleX: 1.2 }, { scaleY: 1.4 }],
        }}
        source={require("../assets/lottieBackground.json")}
        // source={require("../assets/loginscreen.json")}
        autoPlay
        loop={true}
        speed={1}
      />
      <KeyboardAvoidingView
        // behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <TouchableWithoutFeedback
          onPressOut={Keyboard.dismiss}
          onPressIn={Keyboard.dismiss}
          onPress={Keyboard.dismiss}
        >
          <>
            <View style={styles.container}>
              <Image source={require("../assets/vTour.png")}></Image>
            </View>
            <View style={styles.inputContainer}>
              <Input
                placeholder="Email"
                autoFocus
                type="email"
                value={email}
                onChangeText={(text) => setEmail(text)}
              />
              <Input
                placeholder="Password"
                secureTextEntry
                type="password"
                vlaue={password}
                onChangeText={(text) => setPassword(text)}
              />
            </View>
            <TouchableOpacity
              style={styles.loginButton}
              title="Login"
              onPress={login}
            >
              <Text style={styles.text}>Login</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.registerButton}
              type="outline"
              onPress={() => navigation.navigate("Register")}
              title="Register"
            >
              <Text style={styles.text}>Register</Text>
            </TouchableOpacity>

            <View style={{ height: 40 }} />
          </>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    </>
  );
};

const styles = StyleSheet.create({
  inputContainer: {
    width: 300,
    marginTop: 20,
    marginBottom: 30,
  },
  loginButton: {
    width: 200,
    marginTop: 30,
    alignItems: "center",
    backgroundColor: Colours.primary,
    padding: 12,
    borderRadius: 10,
  },

  registerButton: {
    width: 200,
    marginTop: 20,
    alignItems: "center",
    backgroundColor: Colours.secondary,
    padding: 12,
    borderRadius: 10,
  },

  text: {
    fontSize: 20,
    fontWeight: "400",
    color: "#696969",
  },

  container: {
    justifyContent: "center",
    alignItems: "center",
  },
});

export default LoginScreen;
