import React, { useEffect, useState } from "react";
import { createStackNavigator } from "@react-navigation/stack";
import InfoScreen from "./InfoScreen";
// import CategoriesScreen from "./CategoriesScreen";
import CategoryScreen from "./CategoryScreen";
import foodImageScreen from "./FoodImageScreen";

const Stack = createStackNavigator();

const foodList = ["Cafe", "Hawker Centres", "Restaurants", "Others"];

function FoodScreen() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Attractions"
        component={foodImageScreen}
        options={{ title: "Choose a Category" }}
        initialParams={{ list: foodList, type: "food-beverages" }}
      />
      <Stack.Screen
        name="Category"
        component={CategoryScreen}
        options={{ title: "Choose a Shop" }}
      />
      <Stack.Screen name="Info" component={InfoScreen} />
    </Stack.Navigator>
  );
}

export default FoodScreen;
