import React, { useState, useLayoutEffect } from "react";
import {
  StyleSheet,
  View,
  KeyboardAvoidingView,
  TextInput,
  ImageBackground,
  Platform,
  TouchableOpacity,
  Text,
} from "react-native";
import { Button, Avatar } from "react-native-elements";
import { auth } from "../Firebase";
import Icon from "react-native-vector-icons/MaterialIcons";
import { MaterialIcons } from "@expo/vector-icons";
import * as ImagePicker from "expo-image-picker";
import Colours from "../components/Theme/Colours";

export default function EditProfileScreen({ navigation, route }) {
  const [image, setImage] = useState(null);
  const [updatedDisplayName, setUpdatedDisplayName] = useState("");
  const [updatedPhotoURL, setUpdatedPhotoURL] = useState("");
  const user = auth.currentUser;

  //CONFIG HEADER
  useLayoutEffect(() => {
    navigation.setOptions = {
      title: "Edit your Profile",
    };
  });

  // UPDATED PPROFILE HANDLER
  const updateProfileHandler = () => {
    if (updatedDisplayName === 0) {
      alert("Username Invalid!");
    }
    user
      .updateProfile({
        displayName: updatedDisplayName,
        photoURL: image || updatedPhotoURL,
      })
      .then(() => {
        setUpdatedDisplayName("");
      })
      .catch((error) => {
        alert(error);
      });
  };

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    console.log(result);

    if (!result.cancelled) {
      setImage(result.uri);
    }
  };

  return (
    <KeyboardAvoidingView style={styles.container}>
      <View
        style={{
          height: 100,
          width: 100,
          borderRadius: 15,
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <ImageBackground
          source={{ uri: user.photoURL }}
          style={{ height: 100, width: 100 }}
          imageStyle={{ borderRadius: 15 }}
        >
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Icon
              name="photo-camera"
              size={35}
              style={{ zIndex: 10 }}
              onPress={pickImage}
            />
          </View>
        </ImageBackground>
      </View>

      <View style={styles.action}>
        <TextInput
          placeholder="Username"
          value={updatedDisplayName}
          onChangeText={(text) => setUpdatedDisplayName(text)}
          style={styles.textInput}
          autoFocus
          style={styles.editProfileInput}
        />
      </View>
      <View style={styles.action}>
        <TextInput
          style={styles.textInput}
          placeholder="Update Bio"
          value={updatedPhotoURL}
          onChangeText={(text) => setUpdatedPhotoURL(text)}
          style={styles.editProfileInput}
        />
      </View>
      <TouchableOpacity
        onPress={updateProfileHandler}
        title="Update Profile"
        style={styles.button}
      >
        <Text style={{ textAlign: "center", color: "white", fontSize: 20 }}>
          Save Profile
        </Text>
      </TouchableOpacity>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-start",
    paddingTop: 30,
  },
  inputContainer: {
    maxWidth: 250,
    marginTop: 20,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
    paddingHorizontal: 40,
  },
  avatarContainer: {
    alignItems: "center",
    marginVertical: 20,
  },
  button: {
    alignSelf: "center",
    width: 250,
    marginTop: 10,
    backgroundColor: Colours.primary,
    padding: 17,
    borderRadius: 10,
  },
  editProfileInput: {
    marginTop: 10,
    // width: "50%",
  },
  action: {
    flexDirection: "row",
    marginTop: 10,
    marginBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#f2f2f2",
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === "ios" ? 0 : -12,
    paddingLeft: 10,
    color: "#333333",
  },
});
