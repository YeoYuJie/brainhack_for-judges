import React, { useLayoutEffect } from "react";
import { StyleSheet } from "react-native";
import HomeScreen from "./HomeScreen";
import FoodScreen from "./FoodScreen";
import FavScreen from "./FavScreen";
import ProfileScreen from "./ProfileScreen";
import Icons from "react-native-vector-icons/AntDesign";
import { Ionicons, MaterialIcons } from "react-native-vector-icons";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import AddScreen from "./AddScreen";

const Tab = createMaterialBottomTabNavigator();

const MainScreen = ({ navigation }) => {
  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  }, [navigation]);

  return (
    <Tab.Navigator
      initialRouteName="Profile"
      barStyle={{ backgroundColor: "white" }}
      tabBarOptions={{
        showLabel: false,
        activeTintColor: "black",
        activeBackgroundColor: "white",
        inactiveBackgroundColor: "white",
      }}
    >
      <Tab.Screen
        name="Fav"
        component={FavScreen}
        options={{
          tabBarIcon: (focused) => (
            <Icons
              name="hearto"
              size={24}
              style={{ color: focused ? "black" : "white" }}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          tabBarIcon: () => <Icons name="home" size={24} />,
        }}
      />
      <Tab.Screen
        name="Add"
        component={AddScreen}
        options={{
          tabBarIcon: () => <MaterialIcons name="add-box" size={24} />,
        }}
      />
      <Tab.Screen
        name="Food"
        component={FoodScreen}
        options={{
          tabBarIcon: (focused) => (
            <Ionicons name="fast-food-outline" size={24} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          tabBarIcon: () => <Icons name="user" size={24} />,
        }}
      />
    </Tab.Navigator>
  );
};

export default MainScreen;

const styles = StyleSheet.create({});
