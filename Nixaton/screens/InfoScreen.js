import React, { useEffect, useLayoutEffect, useState, useRef } from "react";
import {
  ScrollView,
  StyleSheet,
  View,
  Share,
  Dimensions,
  TouchableOpacity,
} from "react-native";
import { Image, Text, Rating, Input, Overlay } from "react-native-elements";
import Review from "../components/Review";
import About from "../components/About";
import { auth, db } from "../Firebase";
import firebase from "firebase";
import { Appbar } from "react-native-paper";
import { API_KEY } from "../config";
// import { API_KEY } from "@env";
import youtube from "../apis/youtube";
import VideoItem from "../components/VideoItem";
import LottieView from "lottie-react-native";
import { MaterialIcons } from "react-native-vector-icons";
import Map from "../components/Map";

const InfoScreen = ({ navigation, route }) => {
  const [image, setImage] = useState([]);
  const [videos, setVideos] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [favorited, setFavorited] = useState(false);
  const [visible, setVisible] = useState(false);

  const result = route.params.result;
  const { width } = Dimensions.get("window");
  const height = width * 0.6;

  // FETCH IMAGES
  const fetchImage = async (index) => {
    if (result.images[index]?.uuid == undefined) return;
    const response = await fetch(
      `https://tih-api.stb.gov.sg/media/v1/image/uuid/${result.images[index].uuid}?apikey=${API_KEY}`
    );
    const data = await response.json();
    setImage((prevImage) => [data.data.url, ...prevImage]);
  };

  // SET IMAGES
  useEffect(() => {
    if (route.params.type === "attractions") {
      try {
        fetchImage(0);
        fetchImage(1);
        fetchVideos();
      } catch {
        (err) => console.log(err);
      }
      setIsLoading(false);
    } else {
      setImage([route.params.result.images[0].url]);
      try {
        fetchVideos();
      } catch {
        (err) => console.log(err);
      }
      setIsLoading(false);
    }
  }, [route]);

  //FETCH VIDEOS
  const fetchVideos = async () => {
    const response = await youtube.get("/search", {
      params: {
        q: result.name,
      },
    });
    setVideos(response.data.items);
  };

  // LIKING FUNCTIONALITY
  const addFav = () => {
    db.collection("favorites")
      .doc(auth.currentUser.uid)
      .collection("favItems")
      .doc(result.uuid)
      .set({
        timestamp: firebase.firestore.FieldValue.serverTimestamp(),
        favItem: route.params,
        favItemImage: image[0],
      })
      .catch((err) => console.log(err));
    setFavorited((prevFav) => !prevFav);
  };

  // CONFIGURE HEADER
  useLayoutEffect(() => {
    navigation.setOptions({
      title: result.name,
      headerStyle: { backgroundColor: "f29191" },
      headerTintStyle: { color: "black" },
      headerTintColor: "black",
    });
  }, [navigation]);

  // SHARING FUNCTION
  async function onShare() {
    try {
      const result = await Share.share({
        message:
          "React Native | A framework for building native apps using React",
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  }

  //DISPLAY REVIEWS
  const displayReviews = result.reviews.map((reviewer, index) => (
    <Review
      key={index}
      authorURL={reviewer.authorURL}
      profilePhoto={reviewer.profilePhoto}
      rating={reviewer.rating}
      text={reviewer.text}
    />
  ));

  //DISPLAY ABOUT
  const displayAbout = (
    <About
      name={result.name}
      rating={result.rating}
      address={result.address}
      mrt={result.nearestMrtStation}
      contact={result.contact.primaryContactNo}
      description={result.description}
    />
  );

  const toggleOverlay = () => {
    setVisible(!visible);
  };

  const animation = useRef();
  const isFirstRun = useRef(true);

  useEffect(() => {
    if (isFirstRun.current) {
      if (favorited) {
        animation.current.play(32, 33);
      } else {
        animation.current.play(86, 87);
      }
      isFirstRun.current = false;
    } else if (favorited) {
      animation.current.play(0, 15);
    } else {
      animation.current.play(86, 87);
    }
  }, [favorited]);

  return (
    <ScrollView style={{ width, height }}>
      <ScrollView
        pagingEnabled
        horizontal
        showsHorizontalScrollIndicator={false}
        style={{ width, height }}
      >
        {videos.map(
          (video, index) =>
            !isLoading && (
              <VideoItem
                video={video}
                key={index}
                height={height}
                width={width}
              />
            )
        )}

        {image.map((i, index) => (
          <Image
            key={index}
            source={{
              uri: !isLoading && (`${i}?apikey=${API_KEY}` || i),
            }}
            style={{ width, height, resizeMode: "cover" }}
          />
        ))}
      </ScrollView>

      <Appbar
        style={{
          backgroundColor: "white",
          flex: 1,
          flexDirection: "row",
          justifyContent: "flex-start",
        }}
      >
        <TouchableOpacity onPress={addFav}>
          <LottieView
            ref={animation}
            style={styles.heart}
            source={require("../assets/like-button.json")}
            autoPlay={false}
            loop={false}
          />
        </TouchableOpacity>
        <Appbar.Action
          onPress={onShare}
          icon="share-variant"
          style={{
            borderColor: "black",
          }}
        />
        <Appbar.Action
          onPress={toggleOverlay}
          icon="map"
          style={{
            borderColor: "black",
          }}
        />
      </Appbar>

      {displayAbout}
      <Rating
        showRating
        fractions={1}
        // onFinishRating={this.ratingCompleted}
        style={{ backgroundColor: "white", paddingVertical: 10 }}
        imageSize={20}
      />
      <Input
        placeholder="Add a Review"
        containerStyle={{ backgroundColor: "white", alignItems: "center" }}
        inputContainerStyle={{ width: 300 }}
        leftIcon={() => <MaterialIcons name="edit" size={20} />}
      />
      <View style={{ padding: 15, backgroundColor: "white" }}>
        <Text style={{ fontSize: 17.5 }}>Reviews</Text>
        {displayReviews}
      </View>
      <Overlay isVisible={visible} onBackdropPress={toggleOverlay}>
        <Map
          latitude={result.location.latitude}
          longitude={result.location.longitude}
        />
      </Overlay>
    </ScrollView>
  );
};

export default InfoScreen;

const styles = StyleSheet.create({
  heart: {
    width: 50,
    height: 50,
    marginRight: 5,
  },
});
