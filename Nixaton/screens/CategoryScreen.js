import React from "react";
import { useState, useEffect } from "react";
import {
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Text,
  ScrollView,
} from "react-native";
import { ListItem } from "react-native-elements";
import { MaterialIcons } from "react-native-vector-icons";
// import { API_KEY } from "@env";
import { API_KEY } from "../config";

const CategoryScreen = ({ navigation, route }) => {
  const [results, setResults] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(
        `https://tih-api.stb.gov.sg/content/v1/${route.params.type}/search?apikey=${API_KEY}&keyword=${route.params.category}`
      );
      const data = await response.json();
      setResults(data.data);
    };
    try {
      fetchData();
      setIsLoading(false);
    } catch {
      (err) => console.log(err);
    }
  }, [route]);

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        {isLoading && <Text>Loading...</Text>}
        {!isLoading &&
          results.map((result, index) => {
            return (
              <ListItem
                key={index}
                onPress={() =>
                  navigation.navigate("Info", {
                    result,
                    type: route.params.type,
                  })
                }
              >
                <ListItem.Content
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <Text>{result.name}</Text>
                  <TouchableOpacity
                    onPress={() =>
                      navigation.navigate("Info", {
                        result,
                        type: route.params.type,
                      })
                    }
                  >
                    <MaterialIcons name="arrow-forward" size={24} />
                  </TouchableOpacity>
                </ListItem.Content>
              </ListItem>
            );
          })}
      </ScrollView>
    </SafeAreaView>
  );
};

export default CategoryScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  textcontainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    padding: 20,
  },
  text: {
    fontSize: 50,
  },
});
