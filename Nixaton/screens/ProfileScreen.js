import React, { useLayoutEffect, useCallback, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Image,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  RefreshControl,
} from "react-native";
import { Card, Divider } from "react-native-elements";
import { createStackNavigator } from "@react-navigation/stack";
import PostScreen from "./PostScreen";
import { auth } from "../Firebase";
import { profileData } from "../ProfileData";
import EditProfileScreen from "./EditProfileScreen";
import Video from "./VideoScreen";
import { MaterialIcons } from "react-native-vector-icons";

const wait = (timeout) => {
  return new Promise((resolve) => setTimeout(resolve, timeout));
};

function ProfileScreen({ navigation }) {
  const { width } = Dimensions.get("window");
  const adjWidth = width * 0.75;
  const height = width * 0.6;
  const [refreshing, setRefreshing] = useState(false);

  //REFRESH FUNCTIONALITY
  const onRefresh = useCallback(() => {
    setRefreshing(true);
    wait(2000).then(() => setRefreshing(false));
  }, []);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <View style={{ marginRight: 20 }}>
          <MaterialIcons name="settings" size={24} />
        </View>
      ),
    });
  });

  //SIGNOUT HANDLER
  const signOut = () => {
    auth
      .signOut()
      .then(() => navigation.replace("Login"))
      .catch((err) => {
        alert(err);
      });
  };

  // RETRIEVE USER INFO
  const user = auth.currentUser;
  let displayName;
  let photoURL;
  if (user) {
    displayName = user.displayName;
    photoURL = user.photoURL;
  }

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>
      <ScrollView
        style={styles.container}
        contentContainerStyle={{
          justifyContent: "center",
          alignItems: "center",
        }}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
        showsVerticalScrollIndicator={false}
      >
        <Image
          style={styles.userImg}
          source={{
            uri: photoURL,
          }}
        />
        <Text style={styles.userName}>
          {displayName}{" "}
          <MaterialIcons name="verified" size={17} color="#42b3f5" />
        </Text>
        <Text style={styles.aboutUser}>Member of Nixaton</Text>
        <View style={styles.userBtnWrapper}>
          <TouchableOpacity
            style={styles.userBtn}
            onPress={() => {
              navigation.navigate("EditProfile");
            }}
          >
            <Text style={styles.userBtnTxt}>Edit</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.userBtn} onPress={signOut}>
            <Text style={styles.userBtnTxt}>Logout</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.userInfoWrapper}>
          <View style={styles.userInfoItem}>
            <Text style={styles.userInfoTitle}>6</Text>
            <Text style={styles.userInfoSubTitle}>Posts</Text>
          </View>
          <Divider orientation="vertical" width={1} />
          <View style={styles.userInfoItem}>
            <Text style={styles.userInfoTitle}>1706</Text>
            <Text style={styles.userInfoSubTitle}>Followers</Text>
          </View>
          <Divider orientation="vertical" width={1} />
          <View style={styles.userInfoItem}>
            <Text style={styles.userInfoTitle}>700</Text>
            <Text style={styles.userInfoSubTitle}>Following</Text>
          </View>
        </View>
        {/* LOAD POSTS */}
        <ScrollView>
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
              marginBottom: 60,
            }}
          >
            <Video width={adjWidth} height={height} />
            {profileData.map((data, index) => (
              <Card key={index} containerStyle={{ borderRadius: 10 }}>
                <TouchableOpacity
                  style={styles.posts}
                  onPress={() =>
                    navigation.navigate("Post", {
                      name: data.name,
                      imageURL: data.imageFile,
                    })
                  }
                >
                  <Image
                    source={data.imageFile}
                    style={{ height, width: adjWidth }}
                    resizeMode="cover"
                  />
                </TouchableOpacity>
              </Card>
            ))}
          </View>
        </ScrollView>
      </ScrollView>
    </SafeAreaView>
  );
}

const Stack = createStackNavigator();

export default function ProfileStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Profile" component={ProfileScreen} />
      <Stack.Screen name="EditProfile" component={EditProfileScreen} />
      <Stack.Screen name="Post" component={PostScreen} />
    </Stack.Navigator>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 20,
  },
  userImg: {
    height: 150,
    width: 150,
    borderRadius: 75,
  },
  userName: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 10,
    marginBottom: 10,
  },
  aboutUser: {
    fontSize: 12,
    fontWeight: "600",
    color: "#666",
    textAlign: "center",
    marginBottom: 10,
  },
  userBtnWrapper: {
    flexDirection: "row",
    justifyContent: "center",
    width: "100%",
    marginBottom: 10,
  },
  userBtn: {
    borderColor: "#2e64e5",
    borderWidth: 2,
    borderRadius: 3,
    paddingVertical: 8,
    paddingHorizontal: 12,
    marginHorizontal: 5,
  },
  userBtnTxt: {
    color: "#2e64e5",
  },
  userInfoWrapper: {
    flexDirection: "row",
    justifyContent: "space-around",
    width: "100%",
    marginVertical: 20,
  },
  userInfoItem: {
    justifyContent: "center",
  },
  userInfoTitle: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 5,
    textAlign: "center",
  },
  userInfoSubTitle: {
    fontSize: 12,
    color: "#666",
    textAlign: "center",
  },
});
