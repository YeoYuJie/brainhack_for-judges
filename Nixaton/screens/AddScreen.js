import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { Camera } from "expo-camera";
import { Button } from "react-native-elements";
import { MaterialIcons } from "react-native-vector-icons";
import AddPhoto from "./AddPhoto";
import { createStackNavigator } from "@react-navigation/stack";

function TakePhoto({ navigation }) {
  const [hasPermission, setHasPermission] = useState(null);
  const [camera, setCamera] = useState(null);
  const [image, setImage] = useState(null);
  const [type, setType] = useState(Camera.Constants.Type.back);

  useEffect(() => {
    (async () => {
      const { status } = await Camera.requestPermissionsAsync();
      setHasPermission(status === "granted");
    })();
  }, []);

  const takePic = async () => {
    if (camera) {
      const data = await camera.takePictureAsync(null);
      setImage(data.uri);
      navigation.navigate("AddPhoto", { image: data.uri });
    }
  };

  if (hasPermission === null) {
    return <View />;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  return (
    <View style={styles.container}>
      <View style={styles.container}>
        <Camera
          ref={(ref) => setCamera(ref)}
          style={styles.camera}
          type={type}
          ratio="1:1"
        />
      </View>
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-evenly",
        }}
      >
        <Button
          type="clear"
          onPress={() => {
            setType(
              type === Camera.Constants.Type.back
                ? Camera.Constants.Type.front
                : Camera.Constants.Type.back
            );
          }}
          buttonStyle={styles.button}
          icon={<MaterialIcons name="flip-camera-android" size={35} />}
        />
        <Button
          buttonStyle={styles.button}
          type="clear"
          onPress={() => takePic()}
          icon={<MaterialIcons name="photo-camera" size={35} />}
        />
      </View>
      {image && <Image source={{ uri: image }} />}
    </View>
  );
}

const Stack = createStackNavigator();

export default function AddScreen({ navigation }) {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Photo" component={TakePhoto} />
      <Stack.Screen name="AddPhoto" component={AddPhoto} options={{}} />
    </Stack.Navigator>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  button: {
    width: 120,
  },
  camera: {
    flex: 1,
    aspectRatio: 1,
  },
});
