export const profileData = [
  {
    name: "Lower Seletar Reservoir",
    imageFile: require("./images/UpperSeletarImg.jpg"),
  },
  {
    name: "Lian Shan Shuang Lin Monastery",
    imageFile: require("./images/Monastery_LianShanShuangLin.jpg"),
  },
  {
    name: "Seok Seng Cafe",
    imageFile: require("./images/SeokSengCafe.jpg"),
  },
  {
    name: "Tuas Pier",
    imageFile: require("./images/TuasPier.jpg"),
  },
  {
    name: "Marina Bay Apple Store",
    imageFile: require("./images/MarinaBayApple.jpg"),
  },
];
