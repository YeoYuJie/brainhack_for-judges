import React from "react";
import { StyleSheet, View } from "react-native";
import { WebView } from "react-native-webview";

const VideoItem = ({ video, height, width }) => {
  if (!video) {
    return <View />;
  }

  const videoSrc = `https://www.youtube.com/embed/${video.id.videoId}`;
  return (
    <WebView
      originWhitelist={["*"]}
      source={{
        html: `<iframe src=${videoSrc} height="560" width="1000"/>`,
      }}
      style={{ height, width }}
    />
  );
};

export default VideoItem;

const styles = StyleSheet.create({});
