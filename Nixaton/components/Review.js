import React from "react";
import { StyleSheet, Alert } from "react-native";
import { ListItem, Avatar, Rating } from "react-native-elements";

const Review = ({ profilePhoto, authorName, rating, text }) => {
  return (
    <ListItem bottomDivider>
      <Avatar
        rounded
        source={{
          uri: profilePhoto || "https://i.redd.it/r0rce9g9mpw51.jpg",
        }}
      />
      <ListItem.Content>
        <ListItem>{authorName}</ListItem>
        <Rating imageSize={20} readonly startingValue={rating} />
        <ListItem.Subtitle
          ellipsizeMode="tail"
          numberOfLines={2}
          onLongPress={() => Alert.alert("Full Review", text)}
        >
          {text}
        </ListItem.Subtitle>
      </ListItem.Content>
    </ListItem>
  );
};

export default Review;

const styles = StyleSheet.create({});
