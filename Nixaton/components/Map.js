import React from "react";
import MapView from "react-native-maps";
import { StyleSheet, View, Dimensions } from "react-native";

export default function Map({ latitude, longitude }) {
  const { width, height } = Dimensions.get("window");
  const adjWidth = width * 0.7;
  const adjHeight = height * 0.5;
  return (
    <View>
      <MapView
        style={{ width: adjWidth, height: adjHeight }}
        initialRegion={{
          latitude,
          longitude,
          longitudeDelta: 0.04,
          latitudeDelta: 0.05,
        }}
      />
    </View>
  );
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: "#fff",
//     alignItems: "center",
//     justifyContent: "center",
//   },
// });
