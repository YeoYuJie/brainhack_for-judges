import React from "react";
import { StyleSheet } from "react-native";
import { List } from "react-native-paper";

const About = ({ name, mrt, address, rating, description, contact }) => {
  return (
    <List.Accordion title="Overview" style={{ backgroundColor: "white" }}>
      <List.Item title="Name" description={name} style={styles.item} />
      <List.Item title="Rating" description={rating} style={styles.item} />
      <List.Item
        title="Address"
        description={
          (address.streetName, address.postalCode) || "Not available"
        }
        style={styles.item}
      />
      <List.Item title="Nearest Mrt" description={mrt} style={styles.item} />
      <List.Item title="Contact" description={contact} style={styles.item} />
      <List.Item
        title="Description"
        description={description}
        style={styles.item}
      />
    </List.Accordion>
  );
};

const styles = StyleSheet.create({
  item: {
    backgroundColor: "white",
  },
});

export default About;
