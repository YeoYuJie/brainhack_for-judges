export default {
  primary: "#94d0cc",
  secondary: "#eec4c4",
  tertiary: "#f29191",
  accent: "#d1d9d9",
};
