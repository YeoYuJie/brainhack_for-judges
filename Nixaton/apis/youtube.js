import axios from "axios";
import { YT_API_KEY } from "../config";

export default axios.create({
  baseURL: "https://www.googleapis.com/youtube/v3",
  params: {
    part: "snippet",
    type: "video",
    maxResults: 1,
    key: YT_API_KEY,
  },
});
